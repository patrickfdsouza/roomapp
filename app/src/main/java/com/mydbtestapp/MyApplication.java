package com.mydbtestapp;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatDelegate;

import com.cloudinary.android.MediaManager;
import com.crashlytics.android.Crashlytics;
import com.mydbtestapp.retro.ApiClient;
import com.mydbtestapp.retro.ApiInterface;

import io.fabric.sdk.android.Fabric;
import java.util.HashMap;
import java.util.Map;

public class MyApplication extends Application {

    private static ApiInterface _client;
    private Context _context;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        _context = getApplicationContext();
//        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        cloudinaryInit();
        // Make sure we use vector drawables
//        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private void cloudinaryInit() {
        Map config = new HashMap();
        config.put("cloud_name", "topnotch");
        config.put("api_key", "324911159375671");
        config.put("api_secret", "D6D7-8yFZonHIhi00L9eA4UoYNI");
        MediaManager.init(this, config);

    }

    public static ApiInterface getClient() {
        if (_client == null) {
            _client = ApiClient.getClient().create(ApiInterface.class);
        }
        return _client;
    }
}
