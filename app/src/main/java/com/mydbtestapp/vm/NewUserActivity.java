package com.mydbtestapp.vm;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;
import com.google.gson.Gson;
import com.mydbtestapp.MyApplication;
import com.mydbtestapp.R;
import com.mydbtestapp.adapters.CommentsAdapter;
import com.mydbtestapp.db.User;
import com.mydbtestapp.utils.Constants;
import com.mydbtestapp.utils.UiHelper;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewUserActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY = "com.example.android.wordlistsql.REPLY";

    @BindView(R.id.profileImage)
    ImageView profileImage;
    @BindView(R.id.nameETV)
    EditText nameETV;
    @BindView(R.id.linkETV)
    EditText linkETV;
    @BindView(R.id.ageETV)
    EditText ageETV;
    @BindView(R.id.locationETV)
    EditText locationETV;
    @BindView(R.id.educationETV)
    EditText educationETV;
    @BindView(R.id.professionETV)
    EditText professionETV;
    @BindView(R.id.incomeETV)
    EditText incomeETV;
    @BindView(R.id.contactETV)
    EditText contactETV;
    @BindView(R.id.rgRequested)
    RadioGroup rgRequested;
    @BindView(R.id.reqMe)
    RadioButton reqMe;
    @BindView(R.id.reqHim)
    RadioButton reqHim;
    @BindView(R.id.reqOther)
    RadioButton reqOther;
    @BindView(R.id.rgContacted)
    RadioGroup rgContacted;
    @BindView(R.id.conNo)
    RadioButton conNo;
    @BindView(R.id.conYes)
    RadioButton conYes;
    @BindView(R.id.conNotAccepted)
    RadioButton conNotAccepted;
    @BindView(R.id.conAcceptedMe)
    RadioButton conAcceptedMe;
    @BindView(R.id.commentsRCView)
    RecyclerView commentsRCView;

    @BindView(R.id.button_save)
    Button saveButton;
    @BindView(R.id.uploadBtn)
    Button uploadBtn;

    private CommentsAdapter adapter;

    private Context thisActivity;

    private User user;
    private Uri currImage;
    private String uploadedImage = "";
    int reqIndex = 0, contactedIndex = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        ButterKnife.bind(this);

        user = new Gson().fromJson(getIntent().getStringExtra("user"), User.class);

        if (user != null)
            setData();
        thisActivity = this;

        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                saveUser();
            }
        });
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.PICK_IMAGE);
            }
        });
        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });
        rgRequested.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.reqMe:
                        reqIndex = 0;
                        break;
                    case R.id.reqHim:
                        reqIndex = 1;
                        break;
                    case R.id.reqOther:
                        reqIndex = 2;
                        break;
                }
            }
        });
        rgContacted.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.conYes:
                        contactedIndex = 0;
                        break;
                    case R.id.conNo:
                        contactedIndex = 1;
                        break;
                    case R.id.conNotAccepted:
                        contactedIndex = 2;
                        break;
                    case R.id.conAcceptedMe:
                        contactedIndex = 3;
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.PICK_IMAGE) {
            Uri selectedImageUri = data.getData();
            UiHelper.loadImage(profileImage, selectedImageUri);
            currImage = data.getData();
        }
    }

    private void uploadImage() {
        if (currImage != null) {
            MediaManager.get().upload(currImage).callback(new UploadCallback() {
                @Override
                public void onStart(String requestId) {
                    UiHelper.showToast(thisActivity, "Upload started.");
                    saveButton.setEnabled(false);
                }

                @Override
                public void onProgress(String requestId, long bytes, long totalBytes) {

                }

                @Override
                public void onSuccess(String requestId, Map resultData) {
                    uploadedImage = resultData.get("url").toString();
                    UiHelper.showToast(thisActivity, uploadedImage);
                    saveButton.setEnabled(true);
                }

                @Override
                public void onError(String requestId, ErrorInfo error) {
                    saveButton.setEnabled(true);
                }

                @Override
                public void onReschedule(String requestId, ErrorInfo error) {

                }
            }).dispatch();
        }
    }

    private void setData() {
        UiHelper.loadImage(profileImage, user.getImage());
        nameETV.setText(user.getName());
        linkETV.setText(user.getLink());
        ageETV.setText(String.valueOf(user.getAge()));
        locationETV.setText(user.getLocation());
        educationETV.setText(user.getEducation());
        professionETV.setText(user.getProfession());
        incomeETV.setText(user.getIncome());
        contactETV.setText(user.getContact());
        if (user.getComments() != null && user.getComments().size() > 0) {
            commentsRCView.setVisibility(View.VISIBLE);
            commentsRCView.setLayoutManager(new LinearLayoutManager(thisActivity));
            adapter = new CommentsAdapter(this, user.getComments());
            commentsRCView.setAdapter(adapter);
        } else {
            commentsRCView.setVisibility(View.VISIBLE);
        }
        switch (user.getRequested()) {
            case 0:
                reqMe.setChecked(true);
                break;
            case 1:
                reqHim.setChecked(true);
                break;
            case 2:
                reqOther.setChecked(true);
                break;
        }
        switch (user.getContacted()) {
            case 0:
                conNo.setChecked(true);
                break;
            case 1:
                conYes.setChecked(true);
                break;
            case 2:
                conNotAccepted.setChecked(true);
                break;
            case 3:
                conAcceptedMe.setChecked(true);
                break;
        }
    }

    private void saveUser() {
        boolean validation = true;
        if (TextUtils.isEmpty(nameETV.getText()))
            validation = false;
        if (TextUtils.isEmpty(linkETV.getText()))
            validation = false;
        if (TextUtils.isEmpty(locationETV.getText()))
            validation = false;
        if (TextUtils.isEmpty(educationETV.getText()))
            validation = false;
        if (TextUtils.isEmpty(professionETV.getText()))
            validation = false;
        if (TextUtils.isEmpty(incomeETV.getText()))
            validation = false;
        if (TextUtils.isEmpty(contactETV.getText()))
            validation = false;
        if (validation) {
            Call<String> call;
            int id = 0;
            if (user != null)
                id = user.getId();
            if (uploadedImage.isEmpty() && user != null)
                uploadedImage = user.getImage();
            user = new User(id, nameETV.getText().toString(), uploadedImage, linkETV.getText().toString(), Integer.parseInt(ageETV.getText().toString()), locationETV.getText().toString(),
                    educationETV.getText().toString(), professionETV.getText().toString(), incomeETV.getText().toString(),
                    contactETV.getText().toString(), reqIndex, contactedIndex, null, null);
            if (id == 0)
                call = MyApplication.getClient().createUser(new Gson().toJson(user));
            else
                call = MyApplication.getClient().updateUser(new Gson().toJson(user));

            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response != null) {
                        switch (response.code()) {
                            case Constants.CODE_OK:
                                UiHelper.showToast(thisActivity, "User Created");
                                finish();
                                break;
                            case Constants.CODE_BAD_REQUEST:
                                UiHelper.showToast(thisActivity, "Data is incomplete.");
                                break;
                            case Constants.CODE_UNAVAILABLE:
                                UiHelper.showToast(thisActivity, "Unable to create User.");
                                break;
                        }
                    } else
                        UiHelper.showToast(thisActivity, "No response.");
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    UiHelper.showToast(thisActivity, "Call Failed");
                }
            });
        } else {
            UiHelper.showToast(thisActivity, "Please enter all data");
        }
    }
}