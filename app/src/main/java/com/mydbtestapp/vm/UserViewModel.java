package com.mydbtestapp.vm;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.mydbtestapp.db.User;
import com.mydbtestapp.repo.UserRepo;

import java.util.List;

public class UserViewModel extends AndroidViewModel {

    private UserRepo userRepo;
    private LiveData<List<User>> allUsers;


    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepo = new UserRepo(application);
        allUsers = userRepo.getAllUsers();
//        allUsers.observe(this, new Observer<List<User>>() {
//            @Override
//            public void onChanged(@Nullable List<User> users) {
//
//            }
//        });
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }

    public void insert(User user) {
        userRepo.insert(user);
    }
}
