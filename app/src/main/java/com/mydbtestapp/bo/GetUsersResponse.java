package com.mydbtestapp.bo;

import com.mydbtestapp.db.User;

import java.util.List;

public class GetUsersResponse {

    private List<User> users;

    public List<User> getUsers() {
        return users;
    }
}
