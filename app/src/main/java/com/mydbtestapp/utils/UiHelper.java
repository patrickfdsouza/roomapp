package com.mydbtestapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.widget.ImageView;
import android.widget.Toast;

import com.mydbtestapp.R;
import com.squareup.picasso.Picasso;

public class UiHelper {

    private static final String TAG = UiHelper.class.getSimpleName();

    public static void showToast(Context context, String text) {
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void loadImage(ImageView imageView, String url) {
        if (!url.isEmpty())
            Picasso.get().load(url)
                    .fit().centerCrop()
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.error_image)
                    .into(imageView);
        else
            loadError(imageView);
    }

    public static void loadImage(ImageView imageView, Uri uri) {
        if (uri != null)
            Picasso.get().load(uri)
                    .fit().centerCrop()
                    .placeholder(R.drawable.place_holder)
                    .error(R.drawable.error_image)
                    .into(imageView);
        else
            loadError(imageView);
    }

    private static void loadError(ImageView imageView) {
//        Log.d(TAG, "PlaceHolder");
        Picasso.get().load(R.drawable.error_image)
                .placeholder(R.drawable.error_image)
                .fit().centerInside()
                .into(imageView);
//        imageView.setImageTintList(ColorStateList.valueOf());
    }

    public static int getColor(Context context, int color) {
        return context.getResources().getColor(color);
    }

    public static ColorStateList getColorStateList(Context context, int color) {
        return ColorStateList.valueOf(getColor(context, color));
    }
}
