package com.mydbtestapp.utils;

import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

public class UrlParser {

    private static final String TAG = UrlParser.class.getSimpleName();

    private class Description extends AsyncTask<String, Void, Void> {
        String desc;

        @Override
        protected Void doInBackground(String... params) {
            try {
                // Connect to the web site
                Document mBlogDocument = Jsoup.connect(params[0]).get();
                // Using Elements to get the Meta data
//                Elements mElementDataSize = mBlogDocument.select("div[class=author-date]");
                Elements mElementDataSize = mBlogDocument.select("div[class=_2nlj _2xc6]");
//                Elements mElementDataSize = mBlogDocument.select("span[^data=profile_name_in_profile_page]");
                // Locate the content attribute
                int mElementSize = mElementDataSize.size();

                for (int i = 0; i < mElementSize; i++) {
//                    Elements mElementAuthorName = mBlogDocument.select("span[class=vcard author post-author test]").select("a").eq(i);
//                    String mAuthorName = mElementAuthorName.text();
//
//                    Elements mElementBlogUploadDate = mBlogDocument.select("span[class=post-date updated]").eq(i);
//                    String mBlogUploadDate = mElementBlogUploadDate.text();

//                    Elements mElementBlogTitle = mBlogDocument.select("h2[class=entry-title]").select("a").eq(i);
                    Elements mElementBlogTitle = mBlogDocument.select("h1[class=_2nlv]").select("a").eq(i);
                    String mBlogTitle = mElementBlogTitle.text();

                    Log.d(TAG, mBlogTitle);
//                    mAuthorNameList.add(mAuthorName);
//                    mBlogUploadDateList.add(mBlogUploadDate);
//                    mBlogTitleList.add(mBlogTitle);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Set description into TextView
        }
    }
}
