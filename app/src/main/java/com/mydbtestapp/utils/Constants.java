package com.mydbtestapp.utils;

public class Constants {

    public static final int PICK_IMAGE = 10;
    public static final int CALL_PHONE = 100;
    //    public static String BASE_URL = "http://patrickfdsouza.000webhostapp.com";
    public static String BASE_URL = "http://192.168.43.29/usersm/";
//    public static String BASE_URL = "http://10.90.10.115/usersm/";

    public static final int CODE_OK = 201;
    public static final int CODE_UNAVAILABLE = 503;
    public static final int CODE_BAD_REQUEST = 400;
}
