package com.mydbtestapp.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.List;

@Entity(tableName = "user")
public class User {

    @PrimaryKey
    private int id;

    @NonNull
    private String name;

    private String image;

    @NonNull
    private String link;

    @NonNull
    private int age;

    @NonNull
    private String location;
    private String education;
    private String profession;
    private String income;
    private String contact;

    @NonNull
    private int requested;
    @NonNull
    private int contacted;
    @NonNull
    private String created;
    @NonNull
    private String modified;

    private List<Comment> comments;

    public User(int id, @NonNull String name, String image, @NonNull String link, @NonNull int age, @NonNull String location, String education, String profession, String income, String contact, @NonNull int requested, @NonNull int contacted, @NonNull String created, @NonNull String modified) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.link = link;
        this.age = age;
        this.location = location;
        this.education = education;
        this.profession = profession;
        this.income = income;
        this.contact = contact;
        this.requested = requested;
        this.contacted = contacted;
        this.created = created;
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    @NonNull
    public String getLink() {
        return link;
    }

    @NonNull
    public int getAge() {
        return age;
    }

    @NonNull
    public String getLocation() {
        return location;
    }

    public String getEducation() {
        return education;
    }

    public String getProfession() {
        return profession;
    }

    public String getIncome() {
        return income;
    }

    public String getContact() {
        return contact;
    }

    @NonNull
    public int getRequested() {
        return requested;
    }

    @NonNull
    public int getContacted() {
        return contacted;
    }

    @NonNull
    public String getCreated() {
        return created;
    }

    @NonNull
    public String getModified() {
        return modified;
    }

    @NonNull
    public List<Comment> getComments() {
        return comments;
    }

    @NonNull
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}