package com.mydbtestapp.db;

import android.arch.persistence.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class CommentsTypeConverter {

    @TypeConverter
    public static List<Comment> stringToList(String commentsStr) {
        return new Gson().fromJson(commentsStr, new TypeToken<List<Comment>>() {
        }.getType());
    }

    @TypeConverter
    public static String listToString(List<Comment> comments) {
        return new Gson().toJson(comments);
    }
}
