package com.mydbtestapp.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "comment")
public class Comment {
    @PrimaryKey
    private int id;

    @NonNull
    private String text;

    private int profile_id;

    @NonNull
    private int c_id;

    @NonNull
    private String created;

    public Comment(int id, @NonNull String text, int profile_id, int c_id, @NonNull String created) {
        this.id = id;
        this.text = text;
        this.profile_id = profile_id;
        this.c_id = c_id;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    @NonNull
    public String getText() {
        return text;
    }

    public int getProfileId() {
        return profile_id;
    }

    public int getCId() {
        return c_id;
    }

    @NonNull
    public String getCreated() {
        return created;
    }
}
