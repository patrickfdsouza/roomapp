package com.mydbtestapp.retro;

import com.mydbtestapp.bo.GetUsersResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

//    @GET("/getUsers.php")
//    Call<List<User>> getUsers();
//
//    @GET("/getUsers.php")
//    Call<JsonObject> getUsersObject();
//
//    @GET("/getUsers.php")
//    Call<String> getUsersString();

    @GET("getUsers.php")
    Call<GetUsersResponse> getUsersResponse();

    @POST("create.php")
    @FormUrlEncoded
    Call<String> createUser(@Field("user") String user);

    @POST("update.php")
    @FormUrlEncoded
    Call<String> updateUser(@Field("user") String user);
}
