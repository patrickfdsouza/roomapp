package com.mydbtestapp.repo;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.mydbtestapp.MyApplication;
import com.mydbtestapp.bo.GetUsersResponse;
import com.mydbtestapp.db.User;
import com.mydbtestapp.dao.UserDao;
import com.mydbtestapp.db.UserDatabase;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepo {
    private String TAG = UserRepo.class.getSimpleName();

    private UserDao mUserDao;
    private LiveData<List<User>> mAllUsers;

    public UserRepo(Application application) {
        mUserDao = UserDatabase.getDatabase(application).userDao();
        mAllUsers = mUserDao.getAllUsers();
        getRemoteUsers();
    }

    public LiveData<List<User>> getAllUsers() {
        return mAllUsers;
    }

    private void getRemoteUsers() {
        Call<GetUsersResponse> call = MyApplication.getClient().getUsersResponse();

        call.enqueue(new Callback<GetUsersResponse>() {
            @Override
            public void onResponse(Call<GetUsersResponse> call, Response<GetUsersResponse> response) {
                Log.d("TAG", response.code() + "");

                if (response.body() != null) {
                    Log.d("TAG", response.body().toString());
                    List<User> users = response.body().getUsers();
                    insertAll(users);
                }
            }

            @Override
            public void onFailure(Call<GetUsersResponse> call, Throwable t) {
                Log.d(TAG, "Call failed");
            }
        });
    }

    public void insert(User user) {
        new insertAsyncTask(mUserDao).execute(user);
    }

    private static class insertAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao mAsyncTaskDao;

        insertAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            mAsyncTaskDao.save(params[0]);
            return null;
        }
    }

    public void insertAll(List<User> users) {
        new insertAllAsyncTask(mUserDao).execute(users);
    }

    private static class insertAllAsyncTask extends AsyncTask<List<User>, Void, Void> {

        private UserDao mAsyncTaskDao;

        insertAllAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final List<User>... params) {
            mAsyncTaskDao.saveAll(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Executor executor = Executors.newSingleThreadExecutor();

            executor.execute(new Runnable() {
                @Override
                public void run() {
                    LiveData<List<User>> myOrders = mAsyncTaskDao.getAllUsers();
                    Log.d("", "Orders nr = " + (myOrders.getValue() == null ? 0 : myOrders.getValue().size()));
                }
            });
        }
    }
}