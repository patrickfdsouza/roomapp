package com.mydbtestapp;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mydbtestapp.adapters.UserListAdapter;
import com.mydbtestapp.db.User;
import com.mydbtestapp.utils.UiHelper;
import com.mydbtestapp.vm.NewUserActivity;
import com.mydbtestapp.vm.UserViewModel;

import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private Activity thisActivity;
    private UserViewModel userViewModel;

    private RecyclerView recyclerView;
    private UserListAdapter adapter;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        thisActivity = this;

        fab = findViewById(R.id.floating_button_add);
        recyclerView = findViewById(R.id.recyclerview);
        adapter = new UserListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        userViewModel = ViewModelProviders.of(this).get(UserViewModel.class);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(thisActivity, NewUserActivity.class));
            }
        });

        setUsers();
    }

    private void setUsers() {
        userViewModel.getAllUsers().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable final List<User> words) {
                // Update the cached copy of the words in the adapter.
                UiHelper.showToast(thisActivity, "Updated");
                adapter.setUsers(words);
            }
        });
    }
}
