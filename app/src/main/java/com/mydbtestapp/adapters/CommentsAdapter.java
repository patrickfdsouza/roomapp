package com.mydbtestapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mydbtestapp.R;
import com.mydbtestapp.db.Comment;

import java.util.List;

import butterknife.BindView;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentHolder> {

    private Context mContext;
    private final LayoutInflater mInflater;
    private List<Comment> mComments; // Cached copy of words

    public CommentsAdapter(Context context, List<Comment> comments) {
        mContext = context;
        mComments = comments;
        mInflater = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public CommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = mInflater.inflate(R.layout.comment_row, parent, false);
        return new CommentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentHolder holder, int position) {
        Comment current = getItem(position);
        holder.bindData(current);
    }

    private Comment getItem(int position) {
        if (mComments != null)
            return null;
        return mComments.get(position);
    }

    @Override
    public int getItemCount() {
        if (mComments != null)
            return mComments.size();
        return 0;
    }

    public class CommentHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.commentText)
        TextView commentText;
        @BindView(R.id.authorText)
        TextView authorText;

        public CommentHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bindData(Comment comment) {
            if (comment != null) {
                commentText.setText(comment.getText());
                authorText.setText(comment.getCId());
            }
        }
    }
}
