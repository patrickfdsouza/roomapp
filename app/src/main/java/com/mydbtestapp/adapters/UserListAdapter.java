package com.mydbtestapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mydbtestapp.R;
import com.mydbtestapp.db.User;
import com.mydbtestapp.utils.UiHelper;
import com.mydbtestapp.vm.NewUserActivity;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profileRoot)
        CardView root;
        @BindView(R.id.profileCL)
        ConstraintLayout profileCL;
        @BindView(R.id.profileImage)
        ImageView profileImage;
        @BindView(R.id.titleTV)
        TextView titleTV;
        @BindView(R.id.subTitleTV1)
        TextView subTitleTV1;
        //        @BindView(R.id.subTitleTV2)
//        TextView subTitleTV2;
        @BindView(R.id.requested)
        TextView requested;
        @BindView(R.id.contacted)
        TextView contacted;
        @BindView(R.id.requestedImage)
        ImageView requestedImage;
        @BindView(R.id.contactedImage)
        ImageView contactedImage;
        @BindView(R.id.commentImage)
        ImageView commentImage;
        @BindView(R.id.commentTV)
        TextView commentTV;

        private UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = getItem(getAdapterPosition());
                    Intent intent = new Intent(mContext, NewUserActivity.class);
                    intent.putExtra("user", new Gson().toJson(user));
                    mContext.startActivity(intent);
                }
            });
        }

        void bindData(User user) {
            if (user != null) {
                UiHelper.loadImage(profileImage, user.getImage());
                titleTV.setText(String.format(Locale.ENGLISH, "%s (%d)", user.getName(), user.getAge()));
                subTitleTV1.setText(String.format(Locale.ENGLISH, "%s, %s, %s", user.getEducation(), user.getProfession(), user.getLocation()));
                if (user.getComments() != null && user.getComments().size() > 0) {
                    commentTV.setText(user.getComments().get(0).getText());
                    commentTV.setVisibility(View.VISIBLE);
                    commentImage.setVisibility(View.VISIBLE);
                } else {
                    commentTV.setVisibility(View.INVISIBLE);
                    commentImage.setVisibility(View.INVISIBLE);
                }
//                subTitleTV2.setText(String.format(Locale.ENGLISH, "%s", user.getContact()));
//                subTitleTV2.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent callIntent = new Intent(Intent.ACTION_CALL);
//                        callIntent.setData(Uri.parse("tel:" + user.getContact()));
//                        mContext.startActivity(callIntent);
//                    }
//                });
                switch (user.getRequested()) {
                    case 0:
                        requestedImage.setImageResource(R.drawable.call_outgoing);
                        requested.setText("We requested");
                        break;
                    default:
                        requestedImage.setImageResource(R.drawable.call_incoming);
                        requested.setText("They requested");
                        break;
                }
                switch (user.getContacted()) {
                    case 0:
                        contactedImage.setImageTintList(UiHelper.getColorStateList(mContext, R.color.colorRed));
                        contacted.setText("Not contacted");
                        break;
                    case 1:
                        contactedImage.setImageTintList(UiHelper.getColorStateList(mContext, R.color.colorPrimary));
                        contacted.setText("Contacted");
                        break;
                    case 2:
                        contactedImage.setImageTintList(UiHelper.getColorStateList(mContext, R.color.colorRed));
                        contacted.setText("Not Accepted");
                        break;
                    case 3:
                        contactedImage.setImageTintList(UiHelper.getColorStateList(mContext, R.color.colorPrimary));
                        contacted.setText("Accepted Me");
                        break;
                    default:
                        contactedImage.setImageTintList(UiHelper.getColorStateList(mContext, R.color.colorPrimary));
                        break;
                }
                String[] contacts = user.getContact().split("/");
                Button prev = null;
                for (String contact : contacts) {
                    prev = addContactButton(contact, prev);
                }
            }
        }

        private Button addContactButton(String contact, Button prev) {
            ConstraintSet set = new ConstraintSet();

            ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(0, ConstraintSet.WRAP_CONTENT);
            Button button = new Button(mContext);
            button.setLayoutParams(layoutParams);
            button.setId(View.generateViewId());
            button.setText(contact);
            profileCL.addView(button);

            set.clone(profileCL);
            set.constrainDefaultWidth(button.getId(), ConstraintSet.MATCH_CONSTRAINT_SPREAD);
//            set.constrainDefaultHeight(button.getId(), ConstraintSet.MATCH_CONSTRAINT_SPREAD);
            set.connect(button.getId(), ConstraintSet.START, profileCL.getId(), ConstraintSet.START);
            set.connect(button.getId(), ConstraintSet.BOTTOM, profileCL.getId(), ConstraintSet.BOTTOM);
            set.connect(button.getId(), ConstraintSet.TOP, commentTV.getId(), ConstraintSet.BOTTOM);
            set.connect(button.getId(), ConstraintSet.END, profileCL.getId(), ConstraintSet.END);
            if (prev != null) {
                set.connect(button.getId(), ConstraintSet.START, prev.getId(), ConstraintSet.END);
                set.connect(prev.getId(), ConstraintSet.END, button.getId(), ConstraintSet.START);
            } else {
//                set.connect(button.getId(), ConstraintSet.START, profileCL.getId(), ConstraintSet.START);
                set.connect(button.getId(), ConstraintSet.END, profileCL.getId(), ConstraintSet.END);
            }

            set.applyTo(profileCL);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + contact));
                    mContext.startActivity(callIntent);
                }
            });

            return button;
        }
    }

    private Context mContext;
    private final LayoutInflater mInflater;
    private List<User> mUsers; // Cached copy of words

    public UserListAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.user_row, parent, false);
        return new UserHolder(itemView);
    }

    @Override
    public void onBindViewHolder(UserHolder holder, int position) {
        User current = getItem(position);
        holder.bindData(current);
    }

    public void setUsers(List<User> users) {
        mUsers = users;
        notifyDataSetChanged();
    }

    private User getItem(int position) {
        if (mUsers != null)
            return mUsers.get(position);
        return null;
    }

    // getItemCount() is called many times, and when it is first called,
    // mUsers has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mUsers != null)
            return mUsers.size();
        else return 0;
    }
}