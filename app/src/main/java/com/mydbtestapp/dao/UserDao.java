package com.mydbtestapp.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mydbtestapp.db.User;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {

    @Insert(onConflict = REPLACE)
    void save(User user);

    @Insert(onConflict = REPLACE)
    void saveAll(List<User> users);

    @Query("DELETE FROM user")
    void deleteAll();

//    @Query("SELECT * from user ORDER BY modified DESC")
//    List<User> getAllUser(int id);

    @Query("SELECT * from user ORDER BY modified DESC")
//    List<User> getAllUsers();
    LiveData<List<User>> getAllUsers();
}